# 16 million centralized fungible token

A plateform to buy, exchange and sell pictures. Only 16 million, get it fast ⸮  
Maybe you will be lucky and get a shiny ✨.

## features

- minimalist
- buy, exchange and sell between users
- random shiny 1/1 million
- owner can rename
- more to come, to keep the hype high...

## technical stack

- web server in go
- db in sqlite
- index in elastisearch
- generate script
- reindex script

### schema

- token : name/location, sha, name temp, value r, value g, value b, current owner, []owners history

- user : pseudo, password, []tokens

